from ch.kri.brad.utility.Primes import primes
import math

value = 600851475143
maxFactor = int(math.sqrt(value))
factors = []

prime = primes()
p = next(prime)
while (p < maxFactor):
    if (value % p == 0): factors.append(p)
    p = next(prime)
    
print(factors)
