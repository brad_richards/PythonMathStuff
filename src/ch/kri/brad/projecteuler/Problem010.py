from ch.kri.brad.utility.Primes import primes

p = primes()
total = 0
nextPrime = next(p)
while (nextPrime < 2000000):
    total += nextPrime
    nextPrime = next(p)
print(total)