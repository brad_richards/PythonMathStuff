# The first problem to require serious optimization!!
#
# We use a dictionary that remembers the length of the chain for all numbers
# that we have seen. As soon as a new number evolves to an entry in the
# dictionary, we know the remaining length. This gives us roughly a factor
# of 10 in speed-up

import time

chainLengths = {}

def collatzLength(val):
    count = 0
    orig = val
    while (val > 1):
        if (val in chainLengths.keys()):
            count += chainLengths[val]
            val = 1
        else:
            if (val % 2 == 0): val //= 2
            else: val = val * 3 + 1
            count += 1
    chainLengths[orig] = count
    return count

answer = 0
length = 0
startTime = time.time()
for n in list(range(1000000)):
    nLen = collatzLength(n)
    if (nLen > length):
        length = nLen
        answer = n 
        print(str(answer) + " has a chain of length " + str(length))
print("Elapsed time: " + str(time.time() - startTime))