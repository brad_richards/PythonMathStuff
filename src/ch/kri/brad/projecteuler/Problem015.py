# The challenge for the problem is the initial analysis:
# 
# First, all paths are symmetric, i.e. paths come in pairs, reflected across the diagonal of the square.
# 
# We can then construct the following table
# 
#    Size
#       1   1                    =   1 * 2 =   2 total paths
#       2   1 + 2                =   3 * 2 =   6 total paths
#       3   1 + 3 + 6            =  10 * 2 =  20 total paths
#       4   1 + 4 + 10 + 20      =  70 * 2 = 140 total paths
#       5   1 + 5 + 15 + 35 + 70 = 126 * 2 = 252 total paths
#       
# In this table, the new element in each row is the result from the previous row. Each of the other elements
# is the sum of the elements in the row above, in the same column and to the left.
# 
# Given this analysis, the implementation below is easy enough...

# Begin with info for size 1
thisGridPaths = 2
thisGridInfo = [1]

for gridSize in list(range(2, 21)):
    # shift
    lastGridInfo = thisGridInfo
    lastGridPaths = thisGridPaths

    # column 0 is always 1
    thisGridInfo = [1]
    thisGridPaths = 1
    
    # Calculate all columns from 1 through next-to-last 
    for entry in list(range(1, gridSize-1)):
        total = sum(lastGridInfo[0:entry+1])
        thisGridInfo.append(total)
        thisGridPaths += total
        
    # Append the last column
    thisGridInfo.append(lastGridPaths)
    thisGridPaths += lastGridPaths
    
    # Double for symmetry
    thisGridPaths *= 2
    
    print(str(thisGridPaths) + ": " + str(thisGridInfo))
    