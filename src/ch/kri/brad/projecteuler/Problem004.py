import math 

def isPalindrome(value):
    # break into digits
    digits = []
    while (value > 0):
        digits.append(value % 10)
        value //= 10
    # compare for result
    pal = True
    pos = 0
    while (pal and pos < len(digits) / 2):
        pal = digits[pos] == digits[len(digits) - 1 - pos]
        pos += 1
    return pal

largest = 0
for x in list(range(1000)):
    for y in list(range(x+1)):
        product = x * y 
        if (isPalindrome(product)):
            if (largest < product): largest = product 
print(largest)