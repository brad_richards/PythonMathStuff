# Generator for triangular numbers
import math

def triangleNumber():
    lastnum = 1;
    tnum = 0;
    while True:
        tnum += lastnum 
        lastnum += 1
        yield tnum

tgen = triangleNumber()

found = False
maxSoFar = 0
while (not found):
    tnum = next(tgen)
    sq_tnum = int(math.sqrt(tnum))
    if ( (sq_tnum * sq_tnum) == tnum):
        numDivisors = -1 # will otherwise be double-counted
    else:
        numDivisors = 0
    for n in list(range(1, sq_tnum+1)):
        if (tnum % n == 0): numDivisors += 2
    if (numDivisors > maxSoFar):
        maxSoFar = numDivisors
        print(str(tnum) + " has divisors " + str(numDivisors))
    found = (numDivisors > 500)
    
    