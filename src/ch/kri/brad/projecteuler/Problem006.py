max = 101

# Generator function for squares of numbers less than max
def squares():
    val = 1
    while (val < max):
        yield val * val
        val += 1

total = sum(range(max))
squareOfSums = total * total

s = squares()
sumOfSquares = sum(s)

diff = squareOfSums - sumOfSquares
print(diff)