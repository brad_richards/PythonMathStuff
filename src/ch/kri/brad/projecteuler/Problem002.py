from ch.kri.brad.utility.Fibonacci import fibonacci    

f = fibonacci() # generator object
sumF = 0
term = next(f)
while (term < 4000000):
    if (term % 2 == 0): sumF += term
    term = next(f)
print(sumF)
