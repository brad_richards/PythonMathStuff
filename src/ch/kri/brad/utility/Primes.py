# This module generates prime numbers using the Sieve of Eratosthenes
# Primes can be returned in sequences, using a generator function,
# or they may be queried individually. In either case, the sieve will
# be managed automatically: Initially, all primes up to 1000, then this
# limit is doubled each time additional primes are requested.
# If the caller knows that a much higher limit is required, the sieve
# can be appropriately initialized using the init() function.

import math

# Interal sieve of booleans (position 0 represents the number 0).
# We ignore positions 0 and 1 throughout this module
_sieve = []

# Prime number generator function.
def primes():
    # Next potential prime number to be returned. We start with 2.
    _nextPosition = 2
    while True:
        # Look for next "True" entry
        while (not isPrime(_nextPosition)):
            _nextPosition += 1
        
        yield _nextPosition
        _nextPosition += 1

# Function to return truth value of position in prime sieve.
# This is a separate function, because it also has the task of
# extending the sieve as required. We (roughly) double the sieve
# at each step, unless the requested position is even higher
def isPrime(position):
    if (position >= len(_sieve)):
        newMax = max(len(_sieve)*2, position+1)
        init(newMax)
    return _sieve[position]
    
# Generate primes up to the given limit, following the Eratosthenes method
def init(limit):
    global _sieve # modify the module-level variable
    _sieve = limit * [True] # initialize all value to True
    for divisor in list(range(2, int(math.sqrt(limit))+1)):
        if (_sieve[divisor]):
            position = divisor * divisor
            while (position < limit):
                _sieve[position] = False
                position += divisor


# Module initialization: generate a list up to 1000
# If called as the main, then print these primes
init(1000)
if __name__ == '__main__':
    for prime in list(range(2, len(_sieve))):
        if (_sieve[prime]): print(prime, end=" ")
    print()