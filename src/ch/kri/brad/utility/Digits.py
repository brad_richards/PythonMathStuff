import math

def numDigits(val):
    digits = 0
    if (val > 0):
        digits = int(math.log10(val))+1
    return digits


def toDigits(val):
    nDigits = numDigits(val)
    digits = [0] * nDigits
    for n in list(range(1, nDigits+1)):
        digits[nDigits - n] = val % 10
        val //= 10
    return digits